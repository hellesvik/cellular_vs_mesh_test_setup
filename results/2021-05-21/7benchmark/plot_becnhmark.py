import pandas as pd
from datetime import datetime
from datetime import time
import csv
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib
from matplotlib.ticker import (MultipleLocator,
                               FormatStrFormatter,
                               AutoMinorLocator)
import sys
import numpy as np
import matplotlib.pylab as pl
import matplotlib.gridspec as gridspec


amber = '#FFBF00'
ice = '#4A85E2'
blood = '#780000'
darkgreen = '#03BF3D'
black = '#000000'

tofile = False
if(tofile):
    matplotlib.use("pgf")
    matplotlib.rcParams.update({
        "pgf.texsystem": "pdflatex",
        'font.family': 'serif',
        'text.usetex': True,
        'pgf.rcfonts': False,
    })


def plot_csv(filename):
    df = pd.read_csv(filename)
    measured = ""
    for label in df.head(0):
        measured = label
    headers = [measured,'timestamp']
    #df = pd.read_csv(filename)
    df.dropna()
    for i in range(len(df["timestamp"])):
        if pd.isna(df["timestamp"][i]):
            #print(i)
            df = df.drop([i])
    #print (df)
    df['timestamp'] = df['timestamp'].map(lambda x: datetime.strptime(str(x), '%H:%M:%S'))
    x = df['timestamp']
    y = df[measured]
    if(measured == "current"):
        y = y/1000
    fig, ax = plt.subplots()
    #print(df)
    #ax.set_xlim(xmin=pd.Timestamp("1900-01-01 16:00:00"),xmax=pd.Timestamp("1900-01-01 19:00:00"))
    #ax.set_ylim(ymin=6,ymax=10)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
    plt.xlabel("Time")
    plt.ylabel("Current [mA]")
    
# beautify the x-labels
    #plt.gcf().autofmt_xdate()
# plot
    plt.plot(x,y,color=ice)
    #plt.bar(x,y,width=0.002,color=ice,edgecolor=black)
    #plt.plot(x,y, '.', markersize=10, label='sin')  
    if(tofile):
        saveto = filename.strip(".csv") + ".pgf"
        saveto = saveto.replace("??Hz_","")
        print(saveto)
        plt.savefig(saveto)
    else:
        plt.show()
 
def plot_csv_multiple(filenames):

    df = list()
    x = list()
    y = list()
    #print(filenames)
    if "node1" in filenames[0]:
        node = 1
    elif "node2" in filenames[0]:
        node = 2
    elif "node3" in filenames[0]:
        node = 3
    elif "node4" in filenames[0]:
        node = 4
    else:
        print("Node num not detected")


    for i in range(len(filenames)):
        print(filenames[i])
        df.append(pd.read_csv(str(filenames[i])))
        measured = ""
        for label in df[i].head(0):
            measured = label
        headers = [measured,'timestamp']
        df[i].dropna()
        for j in range(len(df[i]["timestamp"])):
            if pd.isna(df[i]["timestamp"][j]):
                #print(i)
                df[i] = df[i].drop([j])
        if( isinstance(df[i][measured][0],str)):
            for j in range(len(df[i]["timestamp"])):
                try:
                    df[i][measured][j] = float(df[i][measured][j])
                except:
                    df[i] = df[i].drop([j])
                    



                    #print (df[i])
        df[i]['timestamp'] = df[i]['timestamp'].map(lambda x: datetime.strptime(str(x), '%H:%M:%S'))
        x.append(df[i]['timestamp'])
        y.append(df[i][measured])
        if(measured == "current"):
            y[i] = y[i]/1000


    # Create 2x2 sub plots
    gs = gridspec.GridSpec(2, 2,hspace=0.5)

    pl.figure()

    # ax = pl.subplot(gs[0, 0]) # row 0, col 0
    # ax.set_title("Node "+str(node))
    # ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    # ax.set_xlim(xmin=pd.Timestamp("1900-01-01 16:10:00"),xmax=pd.Timestamp("1900-01-01 19:00:00"))
    # ax.set_ylim(ymin=0,ymax=60)
    # pl.plot(x[0],y[0])
    # #pl.bar(x[0],y[0],width=0.001,color=ice,edgecolor=black)
    # #pl.xlabel("Time")
    # pl.ylabel("Current[mA]")
    # #pl.ylabel("Throughput[kbps]")
    # pl.xticks(rotation=45)

    ax = pl.subplot(gs[0, 1]) # row 0, col 0
    ax.set_title("Node "+str(node))
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax.set_xlim(xmin=pd.Timestamp("1900-01-01 15:30:00"),xmax=pd.Timestamp("1900-01-01 17:30:00"))
    #ax.set_xlim(xmin=pd.Timestamp("1900-01-01 20:00:00"),xmax=pd.Timestamp("1900-01-01 23:30:00"))
    ax.set_ylim(ymin=0,ymax=30)
    #pl.plot(x[1],y[1],'*-',color='orange')
    pl.bar(x[1],y[1],width=0.002,color=amber,edgecolor=black)
    #pl.xlabel("Time")
    #pl.ylabel("Current[mA]")
    pl.ylabel("Throughput[kbps]")
    pl.xticks(rotation=45)

    ax = pl.subplot(gs[1, 0]) # row 0, col 0
    ax.set_title("Node "+str(node))
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax.set_xlim(xmin=pd.Timestamp("1900-01-01 15:30:00"),xmax=pd.Timestamp("1900-01-01 17:30:00"))
    #ax.set_xlim(xmin=pd.Timestamp("1900-01-01 20:00:00"),xmax=pd.Timestamp("1900-01-01 23:30:00"))
    ax.set_ylim(ymin=0,ymax=50)
    #pl.plot(x[2],y[2],'*-',color='red')
    pl.bar(x[2],y[2],width=0.002,color=blood,edgecolor=black)
    pl.xlabel("Time")
    #pl.ylabel("Current[mA]")
    pl.ylabel("Avg Latency[ms]")
    pl.xticks(rotation=45)

    ax = pl.subplot(gs[1, 1]) # row 0, col 0
    ax.set_title("Node "+str(node))
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax.set_xlim(xmin=pd.Timestamp("1900-01-01 15:30:00"),xmax=pd.Timestamp("1900-01-01 17:30:00"))
    #ax.set_xlim(xmin=pd.Timestamp("1900-01-01 20:00:00"),xmax=pd.Timestamp("1900-01-01 23:30:00"))
    ax.set_ylim(ymin=0,ymax=90)
    #pl.plot(x[3],y[3],'*-',color='green')
    pl.bar(x[3],y[3],width=0.002,color=darkgreen,edgecolor=black)
    pl.xlabel("Time")
    #pl.ylabel("Current[mA]")
    pl.ylabel("Max Latency[ms]")
    pl.xticks(rotation=45)


   # Hide x labels and tick labels for top plots and y ticks for right plots.
    #for ax in axs.flat:
        #ax.label_outer()

    
# plot
    #plt.plot(x,y)
    #plt.plot(x,y, '.', markersize=10, label='sin')  
    if(tofile):
        saveto =  input("Save as:") 
        plt.savefig(saveto)
    else:
        plt.show()
     

if __name__ == "__main__":
    if(len(sys.argv) == 2):
        filename = str(sys.argv[1])
        plot_csv(filename)
    else:
        filenames = sys.argv[1:]
        plot_csv_multiple(filenames)

