#! /bash/bin
source set_env_var.sh
sudo apt install python-smbus i2c-tools
echo "dtoverlay=i2c-rtc,ds3231" | sudo tee -a /boot/config.txt 
echo "dtparam=i2c_arm=on" | sudo tee -a /boot/config.txt
sudo cp hwclock-set /lib/udev/hwclock-set
sudo apt-get -y remove fake-hwclock
sudo update-rc.d -f fake-hwclock remove
sudo systemctl disable fake-hwclock
pip3 install -r pip_packages.txt
sudo cp rc.local /etc/
sudo cp Mount.sh /home/pi/
git submodule init && git submodule update
cp current_measurment/current_measurment.py ppk2-api-python
cp current_measurment/save_measurments.py ppk2-api-python
crontab crontab_code
sudo cp current_meas.service /etc/systemd/system/
sudo systemctl enable current_meas.service
cd jlink && sudo cp 99-jlink.rules /etc/udev/rules.d
cd -

