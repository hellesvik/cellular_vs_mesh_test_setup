import os
from os.path import expanduser


home = expanduser('~')
file_mode = 'r'
filename = home + '/node_attr/protocol'
f = open(filename, file_mode)
protocol_data = f.read().splitlines()[0]
f.close()

filename = home + '/node_attr/node_type'
f = open(filename, file_mode)
node_type_data = f.read().splitlines()[0]
f.close()



if protocol_data == "thread":
    if node_type_data == "router":
        os.system("/bin/bash thread_router_init.sh")
    elif node_type_data == "sensor":
        os.system("/bin/bash thread_sensor_init.sh")
elif protocol_data == "zigbee":
    if node_type_data == "router":
        os.system("/bin/bash zigbee_router_init.sh")
    elif node_type_data == "sensor":
        os.system("/bin/bash zigbee_sensor_init.sh")
elif protocol_data == "nbiot":
    os.system("/bin/bash nbiot_init.sh")
elif protocol_data == "ltem":
    os.system("/bin/bash ltem_init.sh")
else:
    print("No protocol detected.")

