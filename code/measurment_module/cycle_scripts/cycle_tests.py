import os
from os.path import expanduser
from time import sleep


home = expanduser('~')
file_mode = 'r'

filename = home + '/node_attr/node'
f = open(filename, file_mode)
node_num= f.read().splitlines()[0]
f.close()

filename = home + '/node_attr/protocol'
f = open(filename, file_mode)
file_data = f.read().splitlines()[0]
f.close()

filename = home + '/node_attr/node_type'
f = open(filename, file_mode)
node_type_data = f.read().splitlines()[0]
f.close()

if(node_type_data == "sensor"):
    #First run simultaneous tests
    if file_data == "thread":
        os.system("/bin/bash thread_cycle.sh")
    elif file_data == "zigbee":
        os.system("/bin/bash zigbee_cycle.sh")
    elif file_data == "nbiot":
        os.system("/bin/bash nbiot_cycle.sh")
    elif file_data == "ltem":
        os.system("/bin/bash ltem_cycle.sh")
    else:
        print("No protocol detected.")

    #Then run delayed test, to run it separatley
    sleep((int(node_num)-1)*60)

    if file_data == "thread":
        os.system("/bin/bash thread_cycle.sh")
    elif file_data == "zigbee":
        os.system("/bin/bash zigbee_cycle.sh")
    elif file_data == "nbiot":
        os.system("/bin/bash nbiot_cycle.sh")
    elif file_data == "ltem":
        os.system("/bin/bash ltem_cycle.sh")
    else:
        print("No protocol detected.")


