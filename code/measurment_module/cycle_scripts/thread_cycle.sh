#!/bin/bash

cd "${MEASURMENT_MODULE_PATH}"
cd ../mesh/benchmark/programming
source flash_thread_benchmark.sh
cd "${MEASURMENT_MODULE_PATH}"
cd ../mesh/openthread/openthread_sensor_node/programming
sleep 2s
source flash_openthread_coap_client.sh
cd "${MEASURMENT_MODULE_PATH}"

