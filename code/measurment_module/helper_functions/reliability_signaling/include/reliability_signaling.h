
#ifndef RELIABILITY_SIGNALING_H__
#define RELIABILITY_SIGNALING_H__

#include <zephyr.h>
#include <drivers/gpio.h>

#define SEND_PIN CONFIG_RELIABILITY_SIGNALING_SEND_PIN
#define RELAY_PIN CONFIG_RELIABILITY_SIGNALING_RELAY_PIN
#define SIGNAL_DURATION CONFIG_RELIABILITY_SIGNALING_SIGNAL_DURATION_SECONDS

void reliability_signaling_init();

/** Toggle SEND_PIN twice when called */
void reliability_signal_send();

/** Toggle RELAY_PIN twice when called */
void reliability_signal_relay();

/** Toggle RELAY_PIN and SEND_PIN twice when called */
void reliability_signal_error();

#endif /**RELIABILITY_SIGNALING_H__**/

