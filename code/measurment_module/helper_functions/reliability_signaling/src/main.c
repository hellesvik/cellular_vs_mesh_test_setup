/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include "reliability_signaling.h"


void main(void) {
    reliability_signaling_init();
    while (1) {
        reliability_signal_send();
        k_sleep(K_SECONDS(2));
        reliability_signal_relay();
    }
}
