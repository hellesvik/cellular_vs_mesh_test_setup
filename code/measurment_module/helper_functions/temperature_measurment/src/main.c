/*
 * Copyright (c) 2018 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: LicenseRef-BSD-5-Clause-Nordic
 */

#include <stdio.h>
#include <string.h>
#include <drivers/uart.h>
#include <zephyr.h>
#include "temperature_measurment.h"

int main(void)
{
    uint8_t ret;
    temperature_measurment_init();
    while (1) {
        ret = temperature_sample();
        if (ret == TEMPERATURE_READ_ERROR) {
            printk("Error in adc sampling: %d\n", ret);
        } else if(ret != TEMPERATURE_READ_WAITING) {
            printk("Temperature: %d\n",ret);
        }

        k_sleep(K_MSEC(500));

    }
}
