#include "temperature_measurment.h"

#define BUFFER_SIZE 1
static int16_t m_sample_buffer[BUFFER_SIZE];

const struct device *adc_dev;
const struct device *gpio0_device;
const struct device *gpio1_device;

static const struct adc_channel_cfg m_1st_channel_cfg = {
    .gain = ADC_GAIN,
    .reference = ADC_REFERENCE,
    .acquisition_time = ADC_ACQUISITION_TIME,
    .channel_id = ADC_1ST_CHANNEL_ID,
#if defined(CONFIG_ADC_CONFIGURABLE_INPUTS)
    .input_positive = ADC_1ST_CHANNEL_INPUT,
#endif
};

void adc_init(){
    int err;

    printk("saadc sampling AIN0 \n");
    printk("Example requires secure_boot to have ");
    printk("SAADC set to non-secure!\n");
    printk("If not; BusFault/UsageFault will be triggered\n");

    adc_dev = device_get_binding("ADC_0");
    if (!adc_dev) {
        printk("device_get_binding ADC_0 failed\n");
        while(1);
    }
    err = adc_channel_setup(adc_dev, &m_1st_channel_cfg);
    if (err) {
        printk("Error in adc setup: %d\n", err);
        while(1);
    }
    /* Trigger offset calibration
     * As this generates a _DONE and _RESULT event
     * the first result will be incorrect.
     */
    NRF_SAADC->TASKS_CALIBRATEOFFSET = 1;

}



void gpio_init(){
    gpio1_device = device_get_binding(CONFIG_TEMPERATURE_POWER_PIN_DEVICE);
    if (gpio1_device  == NULL) {
        printk("Error Initializing GPIO\n");
    } else {
        printk("GPIO Initialized.\n");
    }
    gpio_pin_configure(gpio1_device, CONFIG_TEMPERATURE_POWER_PIN, GPIO_OUTPUT);
    gpio_pin_set(gpio1_device, CONFIG_TEMPERATURE_POWER_PIN, 1);
}

void temperature_measurment_init(){
    gpio_init();
    adc_init();
}

int8_t temperature_sample()
{
    int err;
    int8_t temp;
    static bool first_run = true;
    static uint8_t counter = 0;
    static float sum = 0.0;

    const struct adc_sequence sequence = {
        .channels = BIT(ADC_1ST_CHANNEL_ID),
        .buffer = m_sample_buffer,
        .buffer_size = sizeof(m_sample_buffer),
        .resolution = ADC_RESOLUTION,
    };

    if (!adc_dev) {
        return -1;
    }

    err = adc_read(adc_dev, &sequence);

    /* Print the AIN0 values */
    for (int i = 0; i < BUFFER_SIZE; i++) {
        float adc_voltage = 0;
        adc_voltage = (float)(((float)m_sample_buffer[i] / 1027.0f) *
                3600.0f);
        if(err < 0){
            return TEMPERATURE_READ_ERROR;
        }
        counter ++;
        sum+= adc_voltage-100;
        if(counter > 10){
            adc_voltage = sum/counter;
            counter = 0;
            sum = 0;
            if(first_run){
                first_run = false;
                return TEMPERATURE_READ_WAITING;
            }
            //printk("Measured voltage: %f mV\n", adc_voltage);
            temp = adc_voltage/10;
            return temp;
        }
    }
    return TEMPERATURE_READ_WAITING;

}

