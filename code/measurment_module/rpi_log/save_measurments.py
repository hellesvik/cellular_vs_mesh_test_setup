import datetime
import csv
from time import sleep
import os
from os.path import expanduser


##Write a custom measurment to a csv file. File named based on input and date
#node: String: ID of node
#protocol: String: communication protocol used
#distance: String: Distance of test
#freq: String: Frequency used for the wireless communication
#transmit_power: String: Transmission power used for sending. 
#measurment: String: measurment name
#data: String: data to log
#
#Note: Empty values can be insterted, and will be skipped in filename. Use '' as empty string.
def write_to_csv(node,protocol,distance,freq, transmit_power, measurment, data):
    now_date = datetime.datetime.now().strftime('%Y-%m-%d')
    now_hour = datetime.datetime.now().strftime('%H:%M:%S')
    name_list = [node, protocol, distance, freq, transmit_power, measurment, now_date]

    filename = '' 
    for name in name_list:
        if name != '':
            filename += name + '_'
    filename = filename[:-1] + '.csv'

    file_mode = 'a'
    file_exist = False
    if not os.path.isdir("logs"):
        os.mkdir("logs")
    filename = "logs/" + filename
    if not os.path.isfile(filename):
        file_exist = True
    f = open(filename, file_mode)
    with f:
        data_format = ['timestamp', measurment]
        writer = csv.DictWriter(f, fieldnames=data_format)    
        if file_exist:
            writer.writeheader()
        writer.writerow({'timestamp' : now_hour, measurment:data})


def write_measurment_to_csv(measurment, data):
    home = expanduser('~')
    file_mode = 'r'
    filename = home + '/node_attr/node'
    f = open(filename, file_mode)
    file_data = f.read().splitlines()[0]
    node = 'node' + file_data

    filename = home + '/node_attr/protocol'
    f = open(filename, file_mode)
    file_data = f.read().splitlines()[0]
    protocol = file_data
    
    filename = home + '/node_attr/distance'
    f = open(filename, file_mode)
    file_data = f.read().splitlines()[0]
    distance = file_data

    filename = home + '/node_attr/freq'
    f = open(filename, file_mode)
    file_data = f.read().splitlines()[0]
    freq = file_data

    filename = home + '/node_attr/transmit_power'
    f = open(filename, file_mode)
    file_data = f.read().splitlines()[0]
    transmit_power = file_data
    
    write_to_csv(node,protocol,distance,freq,transmit_power,measurment, data)


