from gpiozero import Button
from time import sleep
from save_measurments import write_measurment_to_csv 


sent = Button(9)
relay = Button(10)

pressed_sent = False
pressed_relay = False

while True:
    if not sent.is_pressed and not pressed_sent:
        pressed_sent = True
        write_measurment_to_csv('reliability_sent',1)
    elif sent.is_pressed:
        pressed_sent = False
    if not relay.is_pressed and not pressed_relay: 
        pressed_relay = True
        write_measurment_to_csv('reliability_relay',1)
    elif relay.is_pressed:
        pressed_relay = False

