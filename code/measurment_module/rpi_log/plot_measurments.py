import sys
import pandas as pd
import plotly.express as px



def plot_csv(filename):
    df = pd.read_csv(filename)
    measured = ""
    for label in df.head(0):
        measured = label
    fig = px.line(df, x='timestamp', y = measured, title = filename)
    fig.show()


if __name__ == "__main__":
    filename = str(sys.argv[1])
    plot_csv(filename)
