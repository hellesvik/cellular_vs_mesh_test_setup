Measurement module is made to work with a Raspberry Pi. Have been tested with Raspberry Pi 3b and Raspberry Pi 1.
To use the measurement mode, use following commands, also found in the setup\_copy.txt file:
```
sudo passwd pi
sudo apt update && sudo apt upgrade && sudo apt install vim git tmux python3 python3-pip
sudo vim /etc/hostname
cd && mkdir git && cd git && git clone https://gitlab.com/hellesvik/thesis_nrf_mesh
cd thesis_nrf_mesh/code/measurment_module
source init.sh
reboot
```


For the measurement module to work, the home folder, typically /home/pi, needs to have a folder named node\_attr.
In this folder, multiple files must exist:

\[filename\] : \[Content of file\] (example value)

node:number of node (1)

node_type:If node is router or sensor (sensor)

protocol:What protocol is used (Thread)

distance:Distance between nodes in test (100m)

freq:Sending frequency of node (2.4GHz)

transmit\_power:Transmission power of node (0dBm)

If a usb drive is plugged into the rPi at startup, after init.sh is run, if a named node\_attr exist on the usb drive, it will be copied contents of this to the rPi. This can be seen in Mount.sh

