import serial
import io
import re
from time import sleep
from datetime import *
from save_measurments import write_measurment_to_csv
from os.path import expanduser

def print_utf8(data):
    print(data.decode('utf8'),end = '')

def print_utf8_list(data_list):
    for data in data_list:
        print_utf8(data)

def in_responses(word, responses):
    for response in responses:
        if word in response.decode('utf8'):
            return True
    return False

def in_response(word, response):
    if word in response.decode('utf8'):
            return True
    return False



def find_between(response,from_char,to_char):
    return response[response.find(from_char)+1:response.find(to_char)]
    
def find_to(response,to_char):
    return response[:response.find(to_char)]

def find_latency_max(responses):
    no_retrans_per = ''
    for response_num in range(len(responses)):
        if in_response('Max',responses[response_num]):
            no_retrans_per = responses[response_num].decode('utf8')
            no_retrans_per = find_between(no_retrans_per ,':','\r')
    no_retrans_per_float = float(no_retrans_per.strip('ms'))
    return no_retrans_per_float
   
def find_latency_avg(responses):
    retrans_per = ''
    for response_num in range(len(responses)):
        if in_response('Avg',responses[response_num]):
            retrans_per = responses[response_num].decode('utf8')
            retrans_per = find_between(retrans_per ,':','\r')
    print(retrans_per.strip('ms'))
    retrans_per_float = float(retrans_per.strip('ms'))
    return retrans_per_float
    
def find_troughput(responses):
    retrans_tru = ''
    for response_num in range(len(responses)):
        if in_response('hput',responses[response_num]):
            retrans_tru = responses[response_num].decode('utf8')
    retrans_tru = find_between(retrans_tru ,':','\r')
    print("AAA")
    print(retrans_tru)
    retrans_tru_int = int(retrans_tru.strip(' kbps'))
    return retrans_tru_int

def peer_discover():
    sleep(1)
    ser.write(b'test peer discover\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    sleep(1)

def get_peer_list():
    peer_discover()
    ser.write(b'test peer list\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    peer_list = list()
    for response in responses:
        response = response.decode('utf8')
        if re.search('\d:',response):
            response = find_between(response,':','\r')
            words = response.split(' ')
            empty_counter = 0
            for word in words:
                if word == '':
                    empty_counter +=1
            for i in range(empty_counter):
                words.remove('')
            long_addr = words[0]
            short_addr = words[1]
            peer_list.append(short_addr)
    return peer_list

def run_test():
    print("Starturd")
    responses = ser.readlines()
    print_utf8_list(responses)
    wait = True
    while wait:
        responses = ser.readlines()
        print_utf8_list(responses)
        if in_responses('Done',responses):
            wait = False

    print(responses)
    avg_latency = find_latency_avg(responses)
    max_latency = find_latency_max(responses)
    troughput = find_troughput(responses)
    print(avg_latency)
    print(max_latency)
    write_measurment_to_csv('avg_latency',avg_latency)
    write_measurment_to_csv('max_latency',max_latency)
    write_measurment_to_csv('troughput',troughput)

home = expanduser('~')
file_mode = 'r'
filename = home + '/node_attr/node'
f = open(filename, file_mode)
file_data = f.read().splitlines()[0]
node = int(file_data)

while(True):
    ser = serial.Serial('/dev/ttyACM0',115200,timeout =0.5)
    ser.write(b'\n')

    run_test()

    ser.close()
