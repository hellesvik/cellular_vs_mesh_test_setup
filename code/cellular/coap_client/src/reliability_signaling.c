#include "reliability_signaling.h"

const struct device *gpio_device;

void reliability_signaling_init(){
    gpio_device = device_get_binding("GPIO_0");
    if (gpio_device  == NULL) {
        printk("Error Initializing GPIO\n");
    } else {
        printk("GPIO Initialized.\n");
    }
    gpio_pin_configure(gpio_device, SEND_PIN, GPIO_OUTPUT);
    gpio_pin_configure(gpio_device, RELAY_PIN, GPIO_OUTPUT);
}

void reliability_signal_send(){
    for(uint8_t i = 0; i < 2; i++){
        gpio_pin_set(gpio_device, SEND_PIN, 1);
        k_sleep(K_SECONDS(SIGNAL_DURATION));
        gpio_pin_set(gpio_device, SEND_PIN, 0);
        k_sleep(K_SECONDS(SIGNAL_DURATION));
    }
}

void reliability_signal_relay(){
    for(uint8_t i = 0; i < 2; i++){
        gpio_pin_set(gpio_device, RELAY_PIN, 1);
        k_sleep(K_SECONDS(SIGNAL_DURATION));
        gpio_pin_set(gpio_device, RELAY_PIN, 0);
        k_sleep(K_SECONDS(SIGNAL_DURATION));
    }
}

void reliability_signal_error(){
    for(uint8_t i = 0; i < 2; i++){
        gpio_pin_set(gpio_device, SEND_PIN, 1);
        gpio_pin_set(gpio_device, RELAY_PIN, 1);
        k_sleep(K_SECONDS(SIGNAL_DURATION));
        gpio_pin_set(gpio_device, SEND_PIN, 0);
        gpio_pin_set(gpio_device, RELAY_PIN, 0);
        k_sleep(K_SECONDS(SIGNAL_DURATION));
    }
}
