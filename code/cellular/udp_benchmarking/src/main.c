/*
 * Copyright (c) 2020 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: LicenseRef-Nordic-5-Clause
 */

#include <zephyr.h>
#include <stdio.h>
#include <kernel.h>
#include <stdio.h>
#include <string.h>
#include <modem/lte_lc.h>
#include <net/socket.h>

#include <date_time.h>

#include <random/rand32.h>

#define UDP_IP_HEADER_SIZE 28
#define UDP_MESSAGE_LEN_MAX 1200

static int client_fd;
static struct sockaddr_storage host_addr;
static struct k_delayed_work server_transmission_work;

K_SEM_DEFINE(lte_connected, 0, 1);
K_TIMER_DEFINE(throughput_timer, NULL, NULL);
K_TIMER_DEFINE(latency_timer, NULL, NULL);


//static struct sockaddr_in to;

#define NUMBER_OF_MESSAGES 1000

static int finished = false;
static int aaa = 0;
static int failed =0;
static uint16_t max_latency=0;
static uint32_t sum_latency=0;
static uint16_t sec;
static uint16_t msec;
static char buffer[CONFIG_UDP_DATA_UPLOAD_SIZE_BYTES] = { [ 0 ... 499 ] = 65 };
static void server_transmission_work_fn(struct k_work *work)
{
    int err;
    aaa++;
    k_timer_start(&latency_timer, K_SECONDS(1), K_SECONDS(1));

    //printk("STRLEN: %i\n",strlen(buffer));
    //printk("STRING: %s\n",buffer);
    /*
       printk("Transmitting UDP/IP payload of %d bytes to the ",
       CONFIG_UDP_DATA_UPLOAD_SIZE_BYTES + UDP_IP_HEADER_SIZE);
       printk("IP address %s, port number %d\n",
       CONFIG_UDP_SERVER_ADDRESS_STATIC,
       CONFIG_UDP_SERVER_PORT);
       */
    err = send(client_fd, buffer, sizeof(buffer), 0);
    if (err < 0) {
        printk("Failed to transmit UDP packet, %d\n", errno);
        return;
    }
    //k_sleep(K_MSEC(2));
    err = recv(client_fd, buffer, sizeof(buffer), 0);
    if (err < 0) {
        printk("Failed to receive UDP packet, %d\n", errno);
        failed ++;
    } else {
        //printf("Received: %s\n",buffer);
        sec = k_timer_status_get(&latency_timer);
        msec = 1000- k_timer_remaining_get(&latency_timer);
        uint16_t latency_msec = sec*1000+msec;
        if(latency_msec > max_latency){
            max_latency = latency_msec;
        }
        sum_latency += latency_msec;
    }
    k_timer_stop(&latency_timer);
    if(aaa<NUMBER_OF_MESSAGES){
        k_delayed_work_submit( &server_transmission_work, K_NO_WAIT);
    } else {
        printk("Done\n");
        printk("Number of failed msg: %d\n",failed);
        printk("Max latency: %d\n",max_latency);
        printk("Avg latency: %d\n",sum_latency/(1000-failed));
        sec = k_timer_status_get(&throughput_timer);
        msec = 1000- k_timer_remaining_get(&throughput_timer);
        uint16_t throughput_msec = 1000*sec + msec;
        printk("Time: %d\n",throughput_msec);
        printk("Troughput: %d kbps\n", (CONFIG_UDP_DATA_UPLOAD_SIZE_BYTES*NUMBER_OF_MESSAGES/(throughput_msec/1000))/1000);
        k_sleep(K_SECONDS(10));
        finished = true;
    }
}

static void work_init(void)
{
    k_delayed_work_init(&server_transmission_work,
            server_transmission_work_fn);
}

#if defined(CONFIG_NRF_MODEM_LIB)
static void lte_handler(const struct lte_lc_evt *const evt)
{
    switch (evt->type) {
        case LTE_LC_EVT_NW_REG_STATUS:
            if ((evt->nw_reg_status != LTE_LC_NW_REG_REGISTERED_HOME) &&
                    (evt->nw_reg_status != LTE_LC_NW_REG_REGISTERED_ROAMING)) {
                break;
            }

            printk("Network registration status: %s\n",
                    evt->nw_reg_status == LTE_LC_NW_REG_REGISTERED_HOME ?
                    "Connected - home network" : "Connected - roaming\n");
            k_sem_give(&lte_connected);
            break;
        case LTE_LC_EVT_PSM_UPDATE:
            printk("PSM parameter update: TAU: %d, Active time: %d\n",
                    evt->psm_cfg.tau, evt->psm_cfg.active_time);
            break;
        case LTE_LC_EVT_EDRX_UPDATE: {
                                         char log_buf[60];
                                         ssize_t len;

                                         len = snprintf(log_buf, sizeof(log_buf),
                                                 "eDRX parameter update: eDRX: %f, PTW: %f\n",
                                                 evt->edrx_cfg.edrx, evt->edrx_cfg.ptw);
                                         if (len > 0) {
                                             printk("%s\n", log_buf);
                                         }
                                         break;
                                     }
        case LTE_LC_EVT_RRC_UPDATE:
                                     printk("RRC mode: %s\n",
                                             evt->rrc_mode == LTE_LC_RRC_MODE_CONNECTED ?
                                             "Connected" : "Idle\n");
                                     break;
        case LTE_LC_EVT_CELL_UPDATE:
                                     printk("LTE cell changed: Cell ID: %d, Tracking area: %d\n",
                                             evt->cell.id, evt->cell.tac);
                                     break;
        default:
                                     break;
    }
}

static int configure_low_power(void)
{
    int err;

#if defined(CONFIG_UDP_PSM_ENABLE)
    /** Power Saving Mode */
    err = lte_lc_psm_req(true);
    if (err) {
        printk("lte_lc_psm_req, error: %d\n", err);
    }
#else
    err = lte_lc_psm_req(false);
    if (err) {
        printk("lte_lc_psm_req, error: %d\n", err);
    }
#endif

#if defined(CONFIG_UDP_EDRX_ENABLE)
    /** enhanced Discontinuous Reception */
    err = lte_lc_edrx_req(true);
    if (err) {
        printk("lte_lc_edrx_req, error: %d\n", err);
    }
#else
    err = lte_lc_edrx_req(false);
    if (err) {
        printk("lte_lc_edrx_req, error: %d\n", err);
    }
#endif

#if defined(CONFIG_UDP_RAI_ENABLE)
    /** Release Assistance Indication  */
    err = lte_lc_rai_req(true);
    if (err) {
        printk("lte_lc_rai_req, error: %d\n", err);
    }
#endif

    return err;
}

static void modem_init(void)
{
    int err;

    if (IS_ENABLED(CONFIG_LTE_AUTO_INIT_AND_CONNECT)) {
        /* Do nothing, modem is already configured and LTE connected. */
    } else {
        err = lte_lc_init();
        if (err) {
            printk("Modem initialization failed, error: %d\n", err);
            return;
        }
    }
}

static void modem_connect(void)
{
    int err;

    if (IS_ENABLED(CONFIG_LTE_AUTO_INIT_AND_CONNECT)) {
        /* Do nothing, modem is already configured and LTE connected. */
    } else {
        err = lte_lc_connect_async(lte_handler);
        if (err) {
            printk("Connecting to LTE network failed, error: %d\n",
                    err);
            return;
        }
    }
}
#endif

static void server_disconnect(void)
{
    (void)close(client_fd);
}

static int server_init(void)
{
    struct sockaddr_in *server4 = ((struct sockaddr_in *)&host_addr);

    server4->sin_family = AF_INET;
    server4->sin_port = htons(CONFIG_UDP_SERVER_PORT);

    inet_pton(AF_INET, CONFIG_UDP_SERVER_ADDRESS_STATIC,
            &server4->sin_addr);

    return 0;
}

static int server_connect(void)
{
    int err;
    client_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (client_fd < 0) {
        printk("Failed to create UDP socket: %d\n", errno);
        goto error;
    }
    struct timeval tv;
    tv.tv_sec = 1;
    tv.tv_usec = 0;
    setsockopt(client_fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

    err = connect(client_fd, (struct sockaddr *)&host_addr,
            sizeof(struct sockaddr_in));
    if (err < 0) {
        printk("Connect failed : %d\n", errno);
        goto error;
    }

    return 0;

error:
    server_disconnect();

    return err;
}

void main(void)
{
    int err;

    printk("UDP sample has started\n");

    work_init();

#if defined(CONFIG_NRF_MODEM_LIB)

    /* Initialize the modem before calling configure_low_power(). This is
     * because the enabling of RAI is dependent on the
     * configured network mode which is set during modem initialization.
     */
    modem_init();

    err = configure_low_power();
    if (err) {
        printk("Unable to set low power configuration, error: %d\n",
                err);
    }

    modem_connect();

    k_sem_take(&lte_connected, K_FOREVER);
#endif

    err = server_init();
    if (err) {
        printk("Not able to initialize UDP server connection\n");
        return;
    }

    err = server_connect();
    if (err) {
        printk("Not able to connect to UDP server\n");
        return;
    }

    //memset(buffer, "A", 500);
    /*
       memset(&to, 0, sizeof(to));
       to.sin_family = AF_INET;
       to.sin_addr   = inet_addr(CONFIG_UDP_SERVER_ADDRESS_STATIC);
       to.sin_port   = htons(CONFIG_UDP_SERVER_PORT);
       k_sleep(K_SECONDS(10));
       */
    /* capture initial time stamp */

    //char buffer[CONFIG_UDP_DATA_UPLOAD_SIZE_BYTES] = {"\0"};
    k_timer_start(&throughput_timer, K_SECONDS(1), K_SECONDS(1));
    k_delayed_work_submit(&server_transmission_work, K_NO_WAIT);

    while(1){
        if(finished){
            aaa = 0;
            finished = false;
            k_sleep(K_MINUTES(15));
            printk("AGAIN\n");
            k_delayed_work_submit(&server_transmission_work, K_NO_WAIT);
        }

        k_sleep(K_MSEC(10));
    }


}
