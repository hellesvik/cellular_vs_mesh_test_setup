/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stdio.h>
#include <zephyr.h>
#include <sys/printk.h>
#include <string.h>
#include <net/net_pkt.h>
#include <openthread/thread.h>
#include <openthread/coap.h>
#include "reliability_signaling.h"
#include "temperature_measurment.h"

#define PANID 0xabcd
#define OT_MASTERKEY_SIZE 16
#if defined(CONFIG_OPENTHREAD_MASTERKEY)
#define OT_MASTERKEY CONFIG_OPENTHREAD_MASTERKEY
#endif

#define COAP_OBSCURE_PORT 5683
//Find COAP_IP_ADDR based on the method explained in Infocenter documentation on Thread Border Router
#define COAP_IP_ADDR "64:ff9b:0:0:0:0:1234:5678"

#define COAP_URI "log"

#define PERIOD 2

static otInstance *instance;
static uint8_t otEui64[8];


void printPayload(otMessage *aMessage){
    uint8_t buf[16];
    uint16_t bytesToPrint;
    uint16_t bytesPrinted = 0;
    uint16_t length = otMessageGetLength(aMessage) - otMessageGetOffset(aMessage);

    if(length > 0) {
        printk("Payload: ");

        while( length > 0){
            bytesToPrint = ( length < sizeof(buf)) ? length : sizeof(buf);
            otMessageRead(aMessage, otMessageGetOffset(aMessage) + bytesPrinted, buf, bytesToPrint);

            printk("%s",buf);

            length -= bytesToPrint;
            bytesPrinted += bytesToPrint;
        }
        printk("\n");
    }
}

void handleResponse(void *aContext, otMessage *aMessage, const otMessageInfo *aMessageInfo, otError aError){

    printPayload(aMessage);
}

void sendCoap(char* data){

    otMessage * message;
    otMessageInfo messageInfo;
    otCoapType coapType = OT_COAP_TYPE_NON_CONFIRMABLE;
    otCoapCode coapCode = OT_COAP_CODE_PUT;
    otIp6Address coapDestinationIp;
    message = otCoapNewMessage(instance,NULL);
    otCoapMessageInit(message,coapType,coapCode);
    otCoapMessageGenerateToken(message,OT_COAP_DEFAULT_TOKEN_LENGTH);
    otCoapMessageAppendUriPathOptions(message, COAP_URI);

    char payload[30];
    otCoapMessageSetPayloadMarker(message);
    sprintf(payload,"%s,%d%d%d%d%d%d%d%d",data,otEui64[0],otEui64[1],otEui64[2],otEui64[3],otEui64[4],otEui64[5],otEui64[6],otEui64[7]);
    printk("Payload to send: %s\n",payload);
    otMessageAppend(message,payload,strlen(payload));

    memset(&messageInfo,0, sizeof(messageInfo));
    otIp6AddressFromString(COAP_IP_ADDR,&coapDestinationIp);
    messageInfo.mPeerAddr = coapDestinationIp;
    messageInfo.mPeerPort = COAP_OBSCURE_PORT;

    error_t error;
    printk("Start Send\n");
    error = otCoapSendRequestWithParameters(instance,message,&messageInfo,&handleResponse,NULL,NULL);
    if((error != OT_ERROR_DROP) && (message != NULL)){
        reliability_signal_error();
        printk("Dropped message\n");
    }
    else if((error != OT_ERROR_NONE) && (message != NULL)){
        reliability_signal_send();
        otMessageFree(message);
        printk("Send successfull\n");
    }
    else{
        printk("Confusion\n");
    } 
}


void initOpenThread(){
    instance = otInstanceInitSingle();
    otMasterKey masterkey;
    if(strlen(OT_MASTERKEY)) {
        net_bytes_from_str(masterkey.m8, OT_MASTERKEY_SIZE, (char *)OT_MASTERKEY);
        otThreadSetMasterKey(instance,&masterkey);
    }
    otLinkSetPanId(instance,PANID);
    otIp6SetEnabled(instance,true);
    otThreadSetEnabled(instance,true);
    otPlatRadioGetIeeeEui64(instance,&otEui64);

}

void initCoap(){
    otCoapStart(instance,COAP_OBSCURE_PORT);
}

void main(void)
{
    reliability_signaling_init();
    temperature_measurment_init();
    initOpenThread();
    initCoap();
    printk("Hello ");

    printk("World!\n");
    char data[20] = "1";
    int8_t power;
    uint16_t a;
    while(1){
        k_sleep(K_SECONDS(10));
        a = otPlatRadioGetTransmitPower(&instance, &power);
        if(a == OT_ERROR_NONE){
            printk("Fine\n");
        }
            printk("AAA: %d\n",power);

        sendCoap(data);
    }

}
