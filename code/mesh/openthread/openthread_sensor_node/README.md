#Openthread CoAP client
This code will measure temperature and send measurments to a cloud server. The messages will be sent using CoAP over openthread.

##Border Router
To give the thread network access to internet, a border router must be set up. Follow the Nordic Border Router [Tutorial](https://infocenter.nordicsemi.com/topic/sdk_tz_v4.1.0/thread_border_router.html). 
In addition to this tutorial, I had to power-cylce the nRF52840dk connected as a openthread NCP, then run 'sudo wpantund' on the pi.

#NCS
The code in this folder is intended for NCS. See the [NCS documentaion](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/nrf/index.html). To build this coap client, this folder may have to be copied into the NCS/nrf/applications directory.

