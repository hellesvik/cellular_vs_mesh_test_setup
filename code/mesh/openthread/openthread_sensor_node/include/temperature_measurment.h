#ifndef TEMPERATURE_MEASURMENT_H_
#define TEMPERATURE_MEASURMENT_H_

#include <zephyr.h>
#include <stdio.h>
#include <string.h>
#include <drivers/gpio.h>
#include <drivers/adc.h>
#include <hal/nrf_saadc.h>
#include <drivers/uart.h>
#define ADC_DEVICE_NAME DT_ADC_0_NAME
#define ADC_RESOLUTION 10
#define ADC_GAIN ADC_GAIN_1_6
#define ADC_REFERENCE ADC_REF_INTERNAL
//#define ADC_ACQUISITION_TIME ADC_ACQ_TIME(ADC_ACQ_TIME_MICROSECONDS, 100)
#define ADC_ACQUISITION_TIME  ADC_ACQ_TIME_DEFAULT
#define ADC_1ST_CHANNEL_ID 0
#define ADC_1ST_CHANNEL_INPUT NRF_SAADC_INPUT_AIN0
#define ADC_2ND_CHANNEL_ID 2
#define ADC_2ND_CHANNEL_INPUT NRF_SAADC_INPUT_AIN2

#define TEMPERATURE_POWER_PIN 15
#define TEMPERATURE_READ_ERROR -120
#define TEMPERATURE_READ_WAITING 120



void adc_init();

void gpio_init();

void temperature_measurment_init();

/** ADC SAMPLING FUNCTION 
 * This function will return the temperature every 10th time it is called.
 * Else it will return -100.
 * It will skip the first 10 resoultions, to get stable readings.
 *
 * Return:
 *      Temperature as an integer
 *      or TEMPERATURE_READ_WAITING
 *      or TEMPERATURE_READ_ERROR
 */
int8_t temperature_sample();
#endif /* TEMPERATURE_MEASURMENT_H_ */
