import serial
import io
import re
from time import sleep

def print_utf8(data):
    print(data.decode('utf8'),end = '')

def print_utf8_list(data_list):
    for data in data_list:
        print_utf8(data)

def in_responses(word, responses):
    for response in responses:
        if word in response.decode('utf8'):
            return True
    return False

def in_response(word, response):
    if word in response.decode('utf8'):
            return True
    return False


ser = serial.Serial('/dev/ttyACM0',115200,timeout =0.5)

def thread_init():
    ser.write(b'ot state\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    if in_responses('disabled',responses):
        ser.write(b'ot ifconfig up\n')
        responses = ser.readlines()
        print_utf8_list(responses)
        ser.write(b'ot thread start\n')
        responses = ser.readlines()
        print_utf8_list(responses)
        sleep(5)
    return

def zigbee_init():
    ser.write(b'bdb role zr\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    if in_responses('Done',responses):
        ser.write(b'bdb start\n')
        responses = ser.readlines()
        print_utf8_list(responses)
        sleep(5)

    ser.write(b'\n')

def benchmark_init():
    ser.write(b'ot\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    if in_responses('Error: no given command',responses):
        print("Thread detected. Running Thread init")
        print("> ",end='')
        thread_init()
        return
    ser.write(b'bdb\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    if in_responses('Please specify a subcommand',responses):
        print("ZigBee detected. Running ZigBee init")
        print("> ",end='')
        zigbee_init()
        return
    print("No compatible protocol found.")
    return

ser.write(b'\n')
benchmark_init()
