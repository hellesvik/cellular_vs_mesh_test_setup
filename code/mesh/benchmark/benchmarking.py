import serial
import io
import re
from time import sleep
from datetime import *
from save_measurments import write_measurment_to_csv
from os.path import expanduser

def print_utf8(data):
    print(data.decode('utf8'),end = '')

def print_utf8_list(data_list):
    for data in data_list:
        print_utf8(data)

def in_responses(word, responses):
    for response in responses:
        if word in response.decode('utf8'):
            return True
    return False

def in_response(word, response):
    if word in response.decode('utf8'):
            return True
    return False

ser = serial.Serial('/dev/ttyACM0',115200,timeout =0.5)
    

def thread_init():
    ser.write(b'ot state\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    if in_responses('disabled',responses):
        ser.write(b'ot ifconfig up\n')
        responses = ser.readlines()
        print_utf8_list(responses)
        ser.write(b'ot thread start\n')
        responses = ser.readlines()
        print_utf8_list(responses)
        sleep(5)
    return

def zigbee_init():
    ser.write(b'bdb role zr\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    if in_responses('Done',responses):
        ser.write(b'bdb start\n')
        responses = ser.readlines()
        print_utf8_list(responses)
        sleep(5)

    ser.write(b'\n')

def benchmark_init():
    print("Starting benchmarking app")
    ser.write(b'ot\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    if in_responses('Error: no given command',responses):
        print("Thread detected. Running Thread init")
        print("> ",end='')
        thread_init()
        return
    ser.write(b'bdb\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    if in_responses('Please specify a subcommand',responses):
        print("ZigBee detected. Running ZigBee init")
        print("> ",end='')
        zigbee_init()
        return
    print("No compatible protocol found.")
    return
     


def find_between(response,from_char,to_char):
    return response[response.find(from_char)+1:response.find(to_char)]
    
def find_to(response,to_char):
    return response[:response.find(to_char)]

def find_latency_max(responses):
    no_retrans_per = ''
    for response_num in range(len(responses)):
        if in_response('Max',responses[response_num]):
            no_retrans_per = responses[response_num].decode('utf8')
            no_retrans_per = find_between(no_retrans_per ,':','\r')
    no_retrans_per_float = float(no_retrans_per.strip('ms'))
    return no_retrans_per_float
    
def find_no_retransmission_troughput(responses):
    no_retrans_tru = ''
    for response_num in range(len(responses)):
        if in_response('Without retransmission',responses[response_num]):
            no_retrans_tru = responses[response_num+2].decode('utf8')
            no_retrans_tru = find_between(no_retrans_tru ,':','\r')
    no_retrans_tru_int = int(no_retrans_tru.strip(' kbps'))
    return no_retrans_tru_int


def find_latency_avg(responses):
    retrans_per = ''
    for response_num in range(len(responses)):
        if in_response('Avg',responses[response_num]):
            retrans_per = responses[response_num].decode('utf8')
            retrans_per = find_between(retrans_per ,':','\r')
    print(retrans_per.strip('ms'))
    retrans_per_float = float(retrans_per.strip('ms'))
    return retrans_per_float
    
def find_retransmission_troughput(responses):
    retrans_tru = ''
    for response_num in range(len(responses)):
        if in_response('With retransmission',responses[response_num]):
            retrans_tru = responses[response_num+2].decode('utf8')
    retrans_tru = find_between(retrans_tru ,':','\r')
    retrans_tru_int = int(retrans_tru.strip(' kbps'))
    return retrans_tru_int

def peer_discover():
    sleep(1)
    ser.write(b'test peer discover\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    sleep(1)

def get_peer_list():
    peer_discover()
    ser.write(b'test peer list\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    peer_list = list()
    for response in responses:
        response = response.decode('utf8')
        if re.search('\d:',response):
            response = find_between(response,':','\r')
            words = response.split(' ')
            empty_counter = 0
            for word in words:
                if word == '':
                    empty_counter +=1
            for i in range(empty_counter):
                words.remove('')
            long_addr = words[0]
            short_addr = words[1]
            peer_list.append(short_addr)
    return peer_list

def run_test(peer):
    ser.write(b'test configure mode echo\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    ser.write(b'test peer select %i\n' % peer)
    responses = ser.readlines()
    print_utf8_list(responses)
    sleep(1)
    if in_responses('Done',responses):
        ser.write(b'\n')
        ser.write(b'test start\n')
        responses = ser.readlines()
        print_utf8_list(responses)
        wait = True
        while wait:
            responses = ser.readlines()
            print_utf8_list(responses)
            if in_responses('Done',responses):
                wait = False

        avg_latency = find_latency_avg(responses)
        print(avg_latency)
        max_latency = find_latency_max(responses)
        retransmission_troughput = find_retransmission_troughput(responses)
        no_retransmission_troughput = find_no_retransmission_troughput(responses)
        write_measurment_to_csv('avg_latency',avg_latency)
        write_measurment_to_csv('max_latency',max_latency)
        write_measurment_to_csv('troughput_retransmitted',retransmission_troughput)
        write_measurment_to_csv('troughput',no_retransmission_troughput)

home = expanduser('~')
file_mode = 'r'
filename = home + '/node_attr/node'
f = open(filename, file_mode)
file_data = f.read().splitlines()[0]
node = int(file_data)

ser.write(b'\n')
benchmark_init()
peer_list = get_peer_list()
print("Peer list:")
print(peer_list)
while (datetime.now().second != 0):
    sleep(0.2)
if(len(peer_list) >0):
    #run_test(0)
    for i in range(len(peer_list)):
        #Find ID of the node you want to test against, and put it below. Has to be found in software, as it can change..
        if peer_list[i] == 'F4CE36F5FBB27B20': 
            run_test(i)
            sleep(int(node)*120)
            run_test(i)
        else:
            print("Chosen partner not here")
else:
    write_measurment_to_csv("troughput","NO nodes available")


ser.close()
