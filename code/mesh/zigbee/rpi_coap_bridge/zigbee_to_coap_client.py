import logging
import asyncio
from aiocoap import *
import serial
from time import sleep
import io
import re

server_address = "8.8.8.8" #Change to address of CoAP server
coap_port = "5683"


peer_addr_short = set()
peer_addr_long = list()
self_addr_short=""
self_addr_long=""

ser = serial.Serial('/dev/ttyACM0',115200,timeout = 0.5)

def print_utf8(data):
    print(data.decode('utf8'),end = '')

def print_utf8_list(data_list):
    for data in data_list:
        print_utf8(data)

def in_responses(word, responses):
    for response in responses:
        if word in response.decode('utf8'):
            return True
    return False

def in_response(word, response):
    if word in response.decode('utf8'):
            return True
    return False

def find_between(response,from_char,to_char):
    return response[response.find(from_char)+1:response.find(to_char)]
    
def find_to(response,to_char):
    return response[:response.find(to_char)]


def zigbee_init():
    ser.write(b'\n')
    ser.write(b'reset\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    sleep(2)
    ser.write(b'bdb role zr\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    ser.write(b'bdb start\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    ser.write(b'zdo eui64\n')
    responses = ser.readlines()
    print_utf8_list(responses)
    global self_addr_long
    self_addr_long =responses[1].decode('utf8')
    self_addr_long = find_to(self_addr_long,'\r')



def find_all_peers_short_addr():
    ser.write(b'zdo match_desc 0xffff 0xffff 0x0104 1 0x0402 0\n')
    wait = True
    peer = ''
    while wait:
        responses = ser.readlines()
        print_utf8_list(responses)
        for response_num in range(len(responses)):
            if in_response('src_addr',responses[response_num]):
                peer = responses[response_num].decode('utf8')
                peer = find_between(peer ,'=',' ')
                peer_addr_short.add(peer)
        if in_responses('Done',responses):
            wait = False

def peer_get_long_addr_all():
    for peer in peer_addr_short:
        cmd = 'zdo ieee_addr ' + peer + '\n'
        ser.write(cmd.encode('ascii'))
        responses = ser.readlines()
        print_utf8_list(responses)
        long_addr = find_between(responses[1].decode('utf8'),'mf4ce','\r')
        peer_addr_long.append(long_addr)

def bind_peer(short_addr, long_addr, endpoint):
    global self_addr_long
    cmd = "zdo bind on " + long_addr + " " + endpoint + " " + self_addr_long + " 64 0x0402 0x" + short_addr + "\n"
    ser.write(cmd.encode('ascii'))
    responses = ser.readlines()
    print_utf8_list(responses)
          
def subscribe_to_peer(long_addr, endpoint):
    cmd = "zcl subscribe on " + long_addr + " " + endpoint + " 0x0402 0x0104 0 41\n"
    ser.write(cmd.encode('ascii'))
    responses = ser.readlines()
    print_utf8_list(responses)
 
def enable_zigbee_callbacks():
    ser.write(b'log enable info zigbee.report\n')
    responses = ser.readlines()
    print_utf8_list(responses)

async def send_to_coap(node,temp):
    context = await Context.create_client_context()
    payload = node + ',' + temp
    payload = payload.encode('ascii')
    print(payload)

    request = Message(code=PUT, payload=payload, uri='coap://'+ server_address + '/'+str(coap_port) +'/log') 
    response = await context.request(request).response
    print('Result: %s\n%r'%(response.code, response.payload))

async def zigbee_to_coap_send():
    flag = False
    while True:
        sleep(0.2)
        try:
            response = ser.readline()
        except:
            print("failed, try again.")
            continue
        node_id=''
        temperature=''
        if flag==False and in_response('node',response):
            print_utf8(response)
            decoded_response = response.decode('utf8','ignore')
            node_id = find_between(decoded_response ,' node ','\\')
            node_id = node_id.strip('node ')
            node_id = re.sub(r'\x1b.*','',node_id)
            flag = True
            response = ser.readline()
        if flag==True and in_response('Value:',response):
            print_utf8(response)
            decoded_response = response.decode('utf8')
            temperature = find_between(decoded_response,'Value: ','\\')
            temperature = temperature.strip('Value: ')
            temperature = re.sub(r'\x1b.*','',temperature)
            await send_to_coap(node_id,temperature)
            flag=False
            

if True:
    print("Start python coap bridge.")
    zigbee_init()
    number=4
    while( len(peer_addr_short) <number):
        peer_addr_short.clear()
        sleep(3)
        find_all_peers_short_addr()
    peer_addr_short = list(peer_addr_short)
    peer_get_long_addr_all()
    for i in range(number):
        bind_peer(peer_addr_short[i], peer_addr_long[i],'1')
        subscribe_to_peer(peer_addr_long[i],'1')
        sleep(1)
    enable_zigbee_callbacks()

if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(zigbee_to_coap_send())

#if __name__ == "__main__":
    #asyncio.get_event_loop().run_until_complete(main())
