/*
 * Copyright (c) 2020 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: LicenseRef-Nordic-5-Clause
 */

/** @file
 *
 * @brief Simple Zigbee light bulb implementation.
 */

#include <zephyr/types.h>
#include <zephyr.h>
#include <device.h>
#include <soc.h>
#include <drivers/pwm.h>
#include <logging/log.h>
#include <dk_buttons_and_leds.h>

#include <zboss_api.h>
#include <zboss_api_addons.h>
#include <zb_mem_config_med.h>
#include <zigbee/zigbee_app_utils.h>
#include <zigbee/zigbee_error_handler.h>
#include <zb_nrf_platform.h>
LOG_MODULE_REGISTER(app);



/**@brief Zigbee stack event handler.
 *
 * @param[in]   bufid   Reference to the Zigbee stack buffer
 *                      used to pass signal.
 */
void zboss_signal_handler(zb_bufid_t bufid)
{
	/* Update network status LED. */
    //zigbee_led_status_update(bufid, ZIGBEE_NETWORK_STATE_LED);

	/* No application-specific behavior is required.
	 * Call default signal handler.
	 */
	ZB_ERROR_CHECK(zigbee_default_signal_handler(bufid));

	/* All callbacks should either reuse or free passed buffers.
	 * If bufid == 0, the buffer is invalid (not passed).
	 */
	if (bufid) {
		zb_buf_free(bufid);
	}
}

void main(void)
{
	//int blink_status = 0;

	//LOG_INF("Starting ZBOSS Light Bulb example");

	/* Initialize */
    //configure_gpio();

	/* Register callback for handling ZCL commands. */
	//ZB_ZCL_REGISTER_DEVICE_CB(zcl_device_cb);

	/* Register dimmer switch device context (endpoints). */
	//ZB_AF_REGISTER_DEVICE_CTX(&dimmable_light_ctx);

	//bulb_clusters_attr_init();
	//level_control_set_value(dev_ctx.level_control_attr.current_level);

	/* Start Zigbee default thread */
	zigbee_enable();

	//LOG_INF("ZBOSS Light Bulb example started");

	while (1) {
		//dk_set_led(RUN_STATUS_LED, (++blink_status) % 2);
	    k_sleep(K_MSEC(50));
        
	}
}
