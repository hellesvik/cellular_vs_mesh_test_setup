#CoAP Server

This CoAP server use (aiocoap)[https://github.com/chrysn/aiocoap], which run on Python 3.
To install aiocoap, follow the steps below. It is recommended to use a python virtual enviroment for the installation. See [venv](https://docs.python.org/3/library/venv.html).

```
git clone https://github.com/chrysn/aiocoap
cd aiocoap
./setup.sh install
```

An example server + client can be run in the two following commands are input into two different terminals:
```
./server.sh
./aiocoap-client coap://localhost/time
```

In the log\_temperature\_server.py file, the variable SERVER\_ADDRESS need to be set to your IP address for the function to work. This project can not promise any security of the CoAP sevrer. 
To run the modified server for logging temperature, copy the log\_temperature\_server.py from this folder into the aiocoap folder. If moving the file does not work, try to copy the contents of log\_temperature\_server.py into the server.py in aiocoap.

This modified version will save a .csv file in its folder.
