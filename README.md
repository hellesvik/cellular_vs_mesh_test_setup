Test setup the masters thesis "Testing of mesh and cellular wireless communication technologies in IoT sensor networks" by Sigurd Hellesvik, TTK4900 at Norwegian University of Technology and Science.

Much of the code here is modified versions of the Nordic Semiconductor examples from Nordic Connect SDK.

Code that required IP addresses require changing of these IP addresses and Ports to function. Now only dummy addresses are used.
